import Collection from './index'

const CollectionPageRouter = () => {
  return <Collection />
}

export default CollectionPageRouter
